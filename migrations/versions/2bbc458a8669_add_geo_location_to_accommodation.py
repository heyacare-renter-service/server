"""add geo location to accommodation

Revision ID: 2bbc458a8669
Revises: 6e4b57a38f4b
Create Date: 2020-12-23 02:10:47.448673

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2bbc458a8669'
down_revision = '6e4b57a38f4b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('accommodation', sa.Column('location_lat', sa.Float(), nullable=True))
    op.add_column('accommodation', sa.Column('location_lon', sa.Float(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('accommodation', 'location_lon')
    op.drop_column('accommodation', 'location_lat')
    # ### end Alembic commands ###
