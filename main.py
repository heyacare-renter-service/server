# coding=utf-8
import logging

import time
import atexit

from apscheduler.schedulers.background import BackgroundScheduler

from app import app

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def print_date_time():
    print(time.strftime("%A, %d. %B %Y %I:%M:%S %p"))


scheduler = BackgroundScheduler()
scheduler.add_job(func=print_date_time, trigger="interval", seconds=60)
scheduler.start()

# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())

if __name__ == '__main__':
    app.run()
