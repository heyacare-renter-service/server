import logging

_logger = logging.getLogger(__name__)

from werkzeug.exceptions import HTTPException as BaseHttpException
from app import models as m
from app.extensions.response_wrapper import wrap_response


class HTTPException(BaseHttpException):
    def __init__(self, code=400, message=None, errors=None, custom_code=None):
        super().__init__(description=message, response=None)
        self.code = code
        self.errors = errors
        self.custom_code = custom_code


class BadRequestException(HTTPException):
    def __int__(self, message='Bad Request', errors=None):
        super().__init__(code=400, message=message, errors=errors)


class NotFoundException(HTTPException):
    def __int__(self, message='Resource Not Found', errors=None):
        super().__init__(code=404, message=message, errors=errors)


class UnAuthorizedException(HTTPException):
    def __int__(self, message='UnAuthorized', errors=None):
        super().__init__(code=401, message=message, errors=errors)


class ForbiddenException(HTTPException):
    def __int__(self, message='Permission Denied', errors=None):
        super().__init__(code=403, message=message, errors=errors)


def global_error_handler(e):
    # traceback.print_exc()
    m.db.session.rollback()
    code = 500
    errors = None
    custom_code = None
    if isinstance(e, BaseHttpException):
        code = e.code
    if isinstance(e, HTTPException):
        errors = e.errors
        custom_code = e.custom_code
    res = wrap_response(None, str(e), code, custom_code)
    if errors:
        res[0]['errors'] = errors
    return res
