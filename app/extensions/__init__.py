import logging

from .response_wrapper import wrap_response
from .namespace import Namespace

_logger = logging.getLogger(__name__)
