import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class Notification(db.Model, TimestampMixin):
    __tablename__ = 'notification'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String(3000), nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    accommodation_id = db.Column(db.String(255), db.ForeignKey('accommodation.id'), nullable=False)
    isRead = db.Column(db.Boolean, nullable=False, default=False)

    user = db.relationship('User', back_populates='notifications')
    accommodation = db.relationship('Accommodation', back_populates='notifications')

    def to_dict(self):
        return {
            'message': self.message,
            'accommodation_id': self.accommodation_id,
            'isRead': self.isRead
        }
