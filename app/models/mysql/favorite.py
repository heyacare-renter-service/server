import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class FavoriteStatus:
    Active = 1
    InActive = 0


class Favorite(db.Model, TimestampMixin):
    __tablename__ = 'favorite'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    accommodation_id = db.Column(db.String(255), db.ForeignKey('accommodation.id'), nullable=False)
    is_active = db.Column(db.Integer, nullable=False, default=True)
    user = db.relationship('User', back_populates='favorites')
    accommodation = db.relationship('Accommodation', back_populates='favorites')

    def to_dict(self):
        return {
            'id': self.id,
            'accommodation': self.accommodation.to_elastic_dict()
        }
