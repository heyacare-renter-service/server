import datetime

from app.commons.constant import DEFAULT_AVATAR
from app.models import db
from app.models.mysql.base import TimestampMixin, UserBase


class UserRole:
    Admin = 0
    Owner = 1
    Renter = 2


class User(db.Model, TimestampMixin, UserBase):
    """
    Contains information of users table.
    """
    __tablename__ = 'user'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    avatar_url = db.Column(db.String(255), default=DEFAULT_AVATAR)
    role = db.Column(db.Integer, default=UserRole.Renter)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    last_login = db.Column(db.TIMESTAMP, nullable=False, default=datetime.datetime.now())
    un_block_at = db.Column(db.TIMESTAMP, nullable=True)

    # relationship:
    accommodations = db.relationship('Accommodation', back_populates='owner')
    ratings = db.relationship('Rating', back_populates='user')
    owner = db.relationship('Owner', back_populates='user')
    notifications = db.relationship('Notification', back_populates='user')
    favorites = db.relationship('Favorite', back_populates='user')

    # sender_reports = db.relationship('Report', back_populates='sender')
    # receiver_reports = db.relationship('Report', back_populates='receiver')

    def to_dict(self):
        return {
            "id": self.id,
            "email": self.email,
            "phone_number": self.phone_number,
            "fullname": self.fullname,
            "avatar_url": self.avatar_url,
            "gender": "Male" if self.gender else "Female",
            "address": self.address
        }

    def to_display_dict(self):
        return {
            "id": self.id,
            "email": self.email,
            "fullname": self.fullname,
            "avatar_url": self.avatar_url,
            "gender": "Male" if self.gender else "Female",
            "address": self.address,
            "role": "owner" if self.role == UserRole.Owner else "renter" if self.role == UserRole.Renter else "admin",
            "phone_number": self.phone_number,
            # "identity_card_number": self.owner.identity_card_number,
        }

    def to_get_admin_dict(self):
        return {
            "id": self.id,
            "avatar_url": self.avatar_url
        }
