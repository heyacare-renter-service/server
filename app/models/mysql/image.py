import logging

from app.models import db
from app.models.mysql.base import TimestampMixin

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


class Image(db.Model, TimestampMixin):
    __tablename__ = 'image'

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    content = db.Column(db.BLOB(length=4294967295))
    name = db.Column(db.String(255), nullable=False, primary_key=True)
    content_type = db.Column(db.String(255), nullable=False)
