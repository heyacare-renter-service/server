import logging

from flask import request
from flask_restplus import Resource

from app import services
from app.extensions import Namespace

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

ns = Namespace('favorite', description='Admin operations')


@ns.route('/get', methods=['POST'])
class GetFavorites(Resource):
    def post(self):
        data = request.args or request.json
        return services.favorite.get_favorite(data['_page'], data['_limit'])


@ns.route('/create', methods=['POST'])
class CreateFavorite(Resource):
    def post(self):
        data = request.args or request.json
        return services.favorite.create_favorite(data['accommodation_id'])


@ns.route('/<favorite_id>/remove', methods=['GET'])
class GetFavorites(Resource):
    def get(self, favorite_id):
        return services.favorite.remove_favorite(favorite_id)
