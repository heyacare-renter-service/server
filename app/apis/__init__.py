import logging

from flask import Blueprint
from flask_restplus import Api

from app.commons.constant import ACCESS_TOKEN_KEY
from app.extensions.exceptions import global_error_handler

_logger = logging.getLogger(__name__)

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': ACCESS_TOKEN_KEY,
        'description': "Type in the *'Value'* input box below: {}: JWT, where JWT is the token".format(ACCESS_TOKEN_KEY)
    }
}

api_bp = Blueprint('apis', __name__, url_prefix='/apis')

apis = Api(
    app=api_bp,
    version='1.0',
    title="HeyaCare's API",
    authorizations=authorizations,
    security='apiKey',
    validate=False,
)


def init_app(app, **kwargs):
    """
    :param app:
    :param kwargs:
    :return:
    """

    from .user import ns as user_ns
    from .register import ns as register_ns
    from .owner import ns as owner_ns
    from .file import ns as file_ns
    from .profile import ns as profile_ns
    from .accommodation import ns as accommodation_ns
    from .admin import ns as admin_ns
    from .favorite import ns as favorite_ns
    from .notification import ns as notification_ns
    from .address import ns as address_ns

    apis.add_namespace(user_ns)
    apis.add_namespace(register_ns)
    apis.add_namespace(owner_ns)
    apis.add_namespace(file_ns)
    apis.add_namespace(profile_ns)
    apis.add_namespace(accommodation_ns)
    apis.add_namespace(admin_ns)
    apis.add_namespace(favorite_ns)
    apis.add_namespace(notification_ns)
    apis.add_namespace(address_ns)

    app.register_blueprint(api_bp)
    apis.error_handlers[Exception] = global_error_handler


from .schema import request
from .schema import response
