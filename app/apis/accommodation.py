# coding=utf-8
import logging

import flask_restplus
from flask import request

import app.apis.schema.request.accommodation
from app.extensions import Namespace
# from app.helpers.validator import validate_accommodation_search_param
from app.services import keyword, accommodation, rating

__author__ = 'nhatns'

_logger = logging.getLogger(__name__)

ns = Namespace('accommodation', description='Accommodation operations')

_accommodation_search_req = ns.model('product_search_req', app.apis.request.accommodation.accommodation_search_req)

_keyword_req = ns.model('keyword_recommend_req', app.apis.request.accommodation.keyword_recommend_req)

_rating_req = ns.model('rating_req', app.apis.request.accommodation.rating_req)
_rating_create_req = ns.model('rating_create_req', app.apis.request.accommodation.rating_create_req)


@ns.route('/search', methods=['POST'])
class SearchAccommodation(flask_restplus.Resource):
    @ns.expect(_accommodation_search_req, validate=True)
    def post(self):
        data = request.args or request.json
        # validate_accommodation_search_param(data)
        return accommodation.get_result_search(data)


@ns.route('/top10-view', methods=['GET'])
class GetTop10View(flask_restplus.Resource):
    def get(self):
        return accommodation.get_top10_view()


@ns.route('/keywords', methods=['POST'])
class KeywordRecommender(flask_restplus.Resource):
    @ns.expect(_keyword_req, validate=True)
    @ns.marshal_with(app.apis.response.accommodation.keyword_recommenders_response)
    def post(self):
        data = request.args or request.json
        return keyword.get_result_search(data)


@ns.route('/<accommodation_id>', methods=['GET'])
class GetAccommodation(flask_restplus.Resource):
    @ns.marshal_with(app.apis.response.accommodation.accommodation_res)
    def get(self, accommodation_id: str):
        return accommodation.get_accommodation_by_id(accommodation_id)


@ns.route('/owner/<accommodation_id>', methods=['GET'])
class GetPreviewRoom(flask_restplus.Resource):
    @ns.marshal_with(app.apis.response.accommodation.accommodation_res)
    def get(self, accommodation_id: str):
        return accommodation.get_room_preview_by_id(accommodation_id)


@ns.route('/owner/<accommodation_id>/for-update', methods=['GET'])
class GetInfoEditRoom(flask_restplus.Resource):
    def get(self, accommodation_id: str):
        return accommodation.get_room_edit_by_id(accommodation_id)


@ns.route('/types', methods=['GET'])
class GetAccommodationTypes(flask_restplus.Resource):
    @ns.marshal_with(app.apis.response.accommodation.accommodation_type, as_list=True)
    def get(self):
        accommodation_types = accommodation.get_type()
        return [accommodation_type.to_dict() for accommodation_type in accommodation_types]


@ns.route('/rating/get', methods=['POST'])
class GetRating(flask_restplus.Resource):
    @ns.expect(_rating_req, validate=True)
    @ns.marshal_with(app.apis.response.accommodation.rating_response)
    def post(self):
        data = request.args or request.json
        return rating.find_rating(**data)


@ns.route('/rating/create', methods=['POST'])
class CreateRating(flask_restplus.Resource):
    @ns.expect(_rating_create_req, validate=True)
    @ns.marshal_with(app.apis.response.accommodation.rating_data)
    def post(self):
        data = request.args or request.json
        return rating.create_rating(**data).to_dict()
