import logging

import flask_restplus
from flask import request

import app.apis.schema.request.owner
import app.apis.schema.response.owner
from app import services
from app.extensions import Namespace

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

ns = Namespace('owner', description='Owner operations')

_owner_register_req = ns.model('owner_register_req', app.apis.schema.request.owner.owner_register_req)
_owner_register_res = ns.model('owner_register_res', app.apis.response.owner.owner_res)

_ingest_data_req = ns.model('ingest_data_req', app.apis.schema.request.owner.ingest_data_req)
_ingest_data_res = ns.model('ingest', app.apis.response.owner.ingest_data_res)

_accommodation_req = ns.model('accommodation_req', app.apis.schema.request.owner.accommodation_req)
_change_rental_status_req = ns.model('change_rental_status_req', app.apis.schema.request.owner.change_rental_status_req)

_view_analysis_req = ns.model('view_analysis_req', app.apis.schema.request.owner.view_analysis_req)

_extend_display_time_req = ns.model('extend_display_time_req', app.apis.schema.request.owner.extend_display_time_req)


@ns.route('/register', methods=['POST'])
class RegisterNewOwner(flask_restplus.Resource):
    @ns.expect(_owner_register_req, validate=True)
    @ns.marshal_with(_owner_register_res)
    def post(self):
        data = request.args or request.json
        register = services.owner.register_to_be_owner(**data)
        services.register.send_confirm_owner_email(**data)
        return register.to_dict()


@ns.route('/accommodation/create', methods=['POST'])
class CreateAccommodation(flask_restplus.Resource):
    @ns.expect(_ingest_data_req, validate=True)
    @ns.marshal_with(_ingest_data_res)
    def post(self):
        data = request.args or request.json
        return services.owner.create_accommodation(**data).to_dict()


@ns.route('/accommodation/update', methods=['PUT'])
class UpdateAccommodation(flask_restplus.Resource):
    @ns.expect(_ingest_data_req, validate=True)
    @ns.marshal_with(_ingest_data_res)
    def put(self):
        data = request.args or request.json
        return services.owner.update_accommodation(**data).to_dict()


@ns.route('/accommodation/get', methods=['POST'])
class GetAccommodation(flask_restplus.Resource):
    @ns.expect(_accommodation_req, validate=True)
    @ns.marshal_with(app.apis.schema.response.owner.accommodations_res)
    def post(self):
        data = request.args or request.json
        return services.owner.get_accommodations(**data)


@ns.route('/accommodation/view/analysis', methods=['POST'])
class GetViewOfAccommodation(flask_restplus.Resource):
    @ns.expect(_view_analysis_req, validate=True)
    @ns.marshal_with(app.apis.schema.response.owner.view_analysis_res)
    def post(self):
        data = request.args or request.json
        return services.owner.get_view_analysis(**data)


@ns.route('/accommodation/change-rental-status', methods=['POST'])
class ChangeRentalStatus(flask_restplus.Resource):
    @ns.expect(_change_rental_status_req, validate=True)
    @ns.marshal_with(app.apis.schema.response.owner.change_rental_status_res)
    def post(self):
        data = request.args or request.json
        return services.owner.change_rental_status(**data)


@ns.route('/accommodation/extend', methods=['POST'])
class ExtendDisplayTime(flask_restplus.Resource):
    @ns.expect(_extend_display_time_req, validate=True)
    def post(self):
        data = request.args or request.json
        return services.owner.extend_display_time(**data)
