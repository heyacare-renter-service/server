import logging

from flask import request
from flask_restplus import Resource

import app.apis.schema.response.user
from app import services
from app.extensions import Namespace

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

ns = Namespace('admin', description='Admin operations')

_get_owner_pending_req = ns.model('owner_pending_req', app.apis.request.admin.get_pending_owner_req)


@ns.route('/owner/pending', methods=['POST'])
class GetPendingOwner(Resource):
    @ns.expect(_get_owner_pending_req, validate=True)
    def post(self):
        data = request.args or request.json
        return {
            'owners': services.admin.get_pending_owner(data),
            'total': services.admin.get_number_pending_owner()
        }


_confirm_owner_req = ns.model('confirm_owner_req', app.apis.request.admin.confirm_owner_req)


@ns.route('/owner/confirm', methods=['POST'])
class ConfirmOwners(Resource):
    @ns.expect(_confirm_owner_req, validate=True)
    def post(self):
        data = request.args or request.json
        ids = data.get('ids')
        return services.admin.confirm_owner_req(ids)


@ns.route('/rating/pending', methods=['POST'])
class GetPendingRating(Resource):
    @ns.expect(_get_owner_pending_req, validate=True)
    @ns.marshal_with(app.apis.schema.response.admin.rating_response)
    def post(self):
        data = request.args or request.json
        return {
            'ratings': services.admin.get_pending_rating(data),
            'total': services.admin.get_total_pending_rating()
        }


@ns.route('/rating/confirm', methods=['POST'])
class ConfirmRatings(Resource):
    @ns.expect(_confirm_owner_req, validate=True)
    def post(self):
        data = request.args or request.json
        ids = data.get('ids')
        return services.admin.confirm_rating_req(ids)


@ns.route('/rating/remove', methods=['POST'])
class RemoveRatings(Resource):
    @ns.expect(_confirm_owner_req, validate=True)
    def post(self):
        data = request.args or request.json
        ids = data.get('ids')
        return services.admin.remove_rating_req(ids)


@ns.route('/accommodation/pending', methods=['POST'])
class GetAccommodation(Resource):
    @ns.expect(_get_owner_pending_req, validate=True)
    def post(self):
        data = request.args or request.json
        return {
            'accommodations': services.admin.get_pending_accommodation(data),
            'total': services.admin.get_total_pending_accommodation()
        }


@ns.route('/accommodation/confirm', methods=['POST'])
class ConfirmAccommodations(Resource):
    def post(self):
        data = request.args or request.json
        ids = data.get('ids')
        return services.admin.confirm_accommodation_req(ids)


@ns.route('/accommodation/decline', methods=['POST'])
class DeclineAccommodations(Resource):
    def post(self):
        data = request.args or request.json
        ids = data.get('ids')
        return services.admin.decline_accommodation_req(ids)


@ns.route('/accommodation/view-analytic', methods=['POST'])
class ViewAnalytics(Resource):
    @ns.marshal_with(app.apis.schema.response.owner.view_analysis_res)
    def post(self):
        data = request.args or request.json
        return services.admin.get_view_analysis()
