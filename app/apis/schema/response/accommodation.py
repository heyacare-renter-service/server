# coding=utf-8
import logging

from flask_restplus import fields

from app.apis import apis

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

_keyword_recommenders_data = apis.model('keyword_recommenders_data', {
    'query': fields.String(attribute='keyword'),
})

keyword_recommenders_response = apis.model('keyword_recommenders_response', {
    'keywords': fields.Nested(_keyword_recommenders_data)
})

_user_data = apis.model('user_data', {
    'id': fields.Integer(),
    'email': fields.String(),
    'phone_number': fields.String(),
    'fullname': fields.String(),
    'avatar_url': fields.String(),
    'gender': fields.String(),
    'address': fields.String()
})

_geo_location_data = apis.model('geo_location_data', {
    'lat': fields.Float(),
    'lon': fields.Float()
})

_geometry_data = apis.model('geometry_data', {
    'location': fields.Nested(_geo_location_data)
})

city = apis.model('city', {
    'code': fields.String(),
    'name': fields.String()
})

district = apis.model('district', {
    'code': fields.String(),
    'name': fields.String()
})

ward = apis.model('ward', {
    'code': fields.String(),
    'name': fields.String()
})

accommodation_res = apis.model('accommodation_res', {
    'id': fields.String(),
    'owner': fields.Nested(_user_data),
    'title': fields.String(),
    'description': fields.String(),
    'street_address': fields.String(),
    'address': fields.String(),
    'accommodation_type': fields.String(),
    'number_of_room': fields.Integer(),
    'price': fields.Integer(),
    'area': fields.Integer(),
    'electricity_price': fields.Integer(),
    'water_price': fields.Integer(),
    'is_stay_with_the_owner': fields.Boolean(),
    'is_rented': fields.Boolean(),
    'bathroom_type': fields.String(),
    'kitchen_type': fields.String(),
    'has_electric_water_heater': fields.Boolean(),
    'has_balcony': fields.Boolean(),
    'has_air_conditioning': fields.Boolean(),
    'number_of_views': fields.Integer(),
    'created_at': fields.String(),
    'images': fields.List(fields.String),
    'nearby_locations': fields.List(fields.String()),
    'geometry': fields.Nested(_geometry_data),
    'city': fields.Nested(city),
    'district': fields.Nested(district),
    'ward': fields.Nested(ward)
})

accommodation_type = apis.model('accommodation_type', {
    'id': fields.Integer(),
    'name': fields.String()
})

rating_data = apis.model('rating_data', {
    'id': fields.Integer(),
    'user_name': fields.String(),
    'star': fields.Integer(),
    'comment': fields.String(),
    'created_at': fields.DateTime()
})

rating_response = apis.model('rating_response', {
    'ratings': fields.Nested(rating_data),
    'average_rating': fields.Float()
})
