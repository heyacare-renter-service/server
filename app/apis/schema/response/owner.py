import logging

from flask_restplus import fields

from app.apis import apis

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

owner_res = {
    'id': fields.String(description='id of owner'),
    'identity_card_number': fields.String(description='identity card number of user'),
    'status': fields.String(description='status of owner')
}

ingest_data_res = {

}

_accommodation_data = apis.model('accommodation_owner_data', {
    'id': fields.String(),
    'title': fields.String(),
    'full_address': fields.String(),
    'number_of_views': fields.Integer(),
    'average_rating': fields.Float(),
    'status': fields.String(),
    'is_rented': fields.Boolean(),
    'created_at': fields.String(),
    'images': fields.List(fields.String()),
    'display_time': fields.DateTime()
})

accommodations_res = apis.model('accommodation_owner_res', {
    'total': fields.Integer(),
    'accommodations': fields.List(fields.Nested(_accommodation_data))
})

change_rental_status_res = {}

view_analysis_res = apis.model('view_analysis_res', {
    'time': fields.String(),
    'value': fields.Integer()
})
