import logging

from flask_restplus import fields

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

get_pending_owner_req = {
    '_page': fields.Integer(required=True, description='paging, default to 1'),
    '_limit': fields.Integer(required=True, description='number of product in response, default to 10')
}

confirm_owner_req = {
    'ids': fields.List(fields.Integer, required=True, description='list of pending owner to confirm')
}