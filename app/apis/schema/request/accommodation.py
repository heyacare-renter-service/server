# coding=utf-8
import logging

from flask_restplus import fields

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

accommodation_search_req = {
    'q': fields.String(required=False, description='Text query to search'),
    'owner_id': fields.Integer(required=False, description='Id of user to fetch accommodation of this seller'),
    'city_code': fields.String(required=False, description='Code of city to fetch accommodation of this city'),
    'district_code': fields.String(required=False,
                                   description='Code of district to fetch accommodation of this district'),
    'ward_code': fields.String(required=False, description='Code of ward to fetch accommodation of this ward'),
    'price_max': fields.Integer(required=False),
    'price_min': fields.Integer(required=False),
    'electricity_price_min': fields.Integer(required=False),
    'electricity_price_max': fields.Integer(required=False),
    'water_price_max': fields.Integer(required=False),
    'water_price_min': fields.Integer(required=False),
    'is_stay_with_the_owner': fields.Boolean(required=False),
    'bathroom_type': fields.Integer(required=False),
    'kitchen_type': fields.Integer(required=False),
    'has_electric_water_heater': fields.Boolean(required=False),
    'has_balcony': fields.Boolean(required=False),
    'has_air_conditioning': fields.Boolean(required=False),
    'sort': fields.List(fields.String, required=False, description="Param to sort  accommodation"),
    '_page': fields.Integer(required=False, description='Paging, default to 1'),
    '_limit': fields.Integer(required=False, description='Number of product in response. Default to 10'),
}

keyword_recommend_req = {
    'q': fields.String(required=False, description='Text query to recommend'),
    '_page': fields.Integer(required=False, description='Paging, default to 1'),
    '_limit': fields.Integer(required=False, description='Number of product in response. Default to 10'),
}

rating_req = {
    'star': fields.Integer(required=False, description='Get rating by star'),
    'accommodation_id': fields.String(required=True, description='Accommodation id to get rating'),
    '_page': fields.Integer(required=False, description='Paging, default to 1'),
    '_limit': fields.Integer(required=False, description='Number of rating in response. Default to 10'),
}

rating_create_req = {
    'star': fields.Integer(required=True, description='Rating star (from 1-5)'),
    'accommodation_id': fields.String(required=True, description='Accommodation id to create rating'),
    'comment': fields.String(required=False, description='Rating comment')
}
