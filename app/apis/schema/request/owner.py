import logging

from flask_restplus import fields

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

owner_register_req = {
    'identity_card_number': fields.String(required=True, description='identity card number of user'),
    'email': fields.String(required=True, description='user email address'),
    'phone_number': fields.String(required=True, description='phone number of user'),
    'fullname': fields.String(required=True, description='fullname of user'),
    'address': fields.String(required=True, description='address of user'),
    'password': fields.String(required=True, description='raw password of user'),
    'gender': fields.Integer(required=False, description='gender of user. 1 -> male')
}

ingest_data_req = {
    'id': fields.String(required=True),
    'owner_id': fields.Integer(required=True),
    'title': fields.String(required=True),
    'description': fields.String(required=True),
    'street_address': fields.String(required=True),
    'ward_code': fields.String(required=True),
    'accommodation_type_id': fields.Integer(required=True),
    'number_of_room': fields.Integer(required=True),
    'price': fields.Integer(required=True),
    'area': fields.Integer(required=True),
    'is_stay_with_the_owner': fields.Boolean(required=True),
    'bathroom_type': fields.Integer(required=True),
    'has_electric_water_heater': fields.Boolean(required=True),
    'kitchen_type': fields.Integer(required=True),
    'has_air_conditioning': fields.Boolean(required=True),
    'has_balcony': fields.Boolean(required=True),
    'electricity_price': fields.Integer(required=True),
    'water_price': fields.Integer(required=True),
    'number_of_days_display': fields.Integer(required=True),
    'images': fields.List(fields.String(), required=True),
    'nearby_locations': fields.List(fields.String())
}

accommodation_req = {
    '_page': fields.Integer(required=False, description='Paging, default to 1'),
    '_limit': fields.Integer(required=False, description='Number of rating in response. Default to 10'),
}

change_rental_status_req = {
    'is_rented': fields.Boolean(required=True),
    'accommodation_id': fields.String(required=True)
}

view_analysis_req = {
    'accommodation_id': fields.String(required=True)
}

extend_display_time_req = {
    'accommodation_id': fields.String(required=True),
    'number_of_days': fields.Integer(required=True)
}
