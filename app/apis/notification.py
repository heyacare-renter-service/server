import logging

from flask_restplus import Resource
from flask import request

from app import services
from app.extensions import Namespace

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

ns = Namespace('notification', description='Admin operations')


@ns.route('/', methods=['POST'])
class GetNotifications(Resource):
    def post(self):
        data = request.args or request.json
        return {
            'notifications': services.notification.get_notifications_by_user_id(data['owner_id'], data['_page'],
                                                                                data['_limit']),
            'number_unread': services.notification.get_number_unread(data['owner_id'])
        }
