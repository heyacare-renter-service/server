import json
from typing import List

from elasticsearch_dsl import query, Search

from app.models.es.accommodation import mappings, settings
from app.repositories.es import build_phrase_prefix_query, build_match_query, build_prefix_query
from app.repositories.es.es_base import EsRepositoryInterface
from config import ACCOMMODATION_INDEX

KEYWORD_NAME_FIELD = 'search_text.raw'
KEYWORD_NAME_NO_TONE_FIELD = 'search_text_no_tone.raw'
SEARCH_TEXT_FIELD = 'search_text'
SEARCH_TEXT_NO_TONE_FIELD = 'search_text_no_tone'


class AccommodationElasticRepo(EsRepositoryInterface):
    def __init__(self):
        super().__init__()
        self._index = ACCOMMODATION_INDEX
        self.mappings = mappings
        self.settings = settings
        self.id_key = 'id'

    def search(self, args):
        """
        Build queries for searching accommodation
        :param args: arguments
        :return: result searching
        """
        have_text_query = args['q'] is not None
        if have_text_query:
            queries = [self.build_first_query(args), self.build_second_query(args)]
            responses, query_index = self.multiple_query(self._index, queries)
        else:
            accommodation_es = self.build_first_query(args)
            responses = accommodation_es.using(self.es).index(self._index).execute()
            responses = responses.to_dict()
        return responses

    def multiple_query(self, index, queries):
        search_queries = []

        for query in queries:
            search_queries.append({
                'index': index
            })
            search_queries.append(query.to_dict())
        query_request = ''
        for search_query in search_queries:
            query_request += '%s \n' % json.dumps(search_query)
        es = self.es
        resp = es.msearch(body=query_request, index=self._index)
        msearch_responses = resp["responses"]
        for query_index, msearch_response in enumerate(msearch_responses):
            if not self.is_empty_responses(msearch_response):
                responses = msearch_response
                return responses, query_index
        return msearch_responses[0], 0

    @staticmethod
    def is_empty_responses(responses):
        number_of_accommodations = responses['hits']['total']['value']
        return number_of_accommodations == 0

    def build_first_query(self, args) -> query.Query:
        search_text = args.get('search_text')
        search_text_no_tone = args.get('q')

        text_query_condition = query.Bool(
            must=[
                query.Bool(
                    should=[query.MatchAll()] if search_text is None else
                    [
                        *self.build_extract_match_text_conditions(search_text, search_text_no_tone, args)
                    ]
                ),
                query.Exists(field="title"),
            ],
            filter=self.get_filter_conditions(args),
        )
        accommodation_es = self.build_accommodation_es_from_text_query_condition(args, text_query_condition)

        return accommodation_es

    def build_second_query(self, args):
        """
        Hàm chỉ được gọi khi có search text
        :param args:
        :param fuzziness:
        :return:
        """
        fuzziness = "AUTO"  # Use auto fuzziness for second query

        search_text = args.get('search_text')

        text_query_condition = query.Bool(
            must=[
                query.Exists(field="title")
            ],
            should=[
                build_match_query(SEARCH_TEXT_FIELD, search_text, fuzziness, 2),
                build_match_query(SEARCH_TEXT_NO_TONE_FIELD, search_text, fuzziness)

            ],
            minimum_should_match=1,
            filter=self.get_filter_conditions(args)
        )

        accommodation_es = self.build_accommodation_es_from_text_query_condition(args, text_query_condition)
        return accommodation_es

    def build_accommodation_es(self, args, accommodation_search_condition, sources):
        accommodation_es = Search() \
            .query(accommodation_search_condition) \
            .source(['id'] if args.get('only_id') else sources) \
            .extra(track_total_hits=True)
        accommodation_es = accommodation_es.sort(*self.sort_condition(args))
        return accommodation_es

    def build_accommodation_es_from_text_query_condition(self, args, text_query_condition):
        accommodation_search_condition = text_query_condition
        sources = self.build_sources(args)
        accommodation_es = self.build_accommodation_es(args, accommodation_search_condition, sources)
        self.add_filter_to_accommodation_es(args, accommodation_es)
        accommodation_es = self.add_page_limit_to_accommodation_es(args, accommodation_es)
        return accommodation_es

    def add_filter_to_accommodation_es(self, args, accommodation_es):
        return accommodation_es

    def add_page_limit_to_es(self, args, accommodation_es):
        _limit = args.get('_limit') if args.get('_limit') else 20
        accommodation_es = accommodation_es[0:_limit]
        return accommodation_es

    @staticmethod
    def add_page_limit_to_accommodation_es(args, accommodation_es):
        _page = args.get('_page') or 1
        _limit = args.get('_limit') or 10
        # Pagination
        accommodation_es = accommodation_es[(_page - 1) * _limit: _page * _limit]
        return accommodation_es

    @staticmethod
    def build_sources(args):
        return ["title", "description", "owner", "full_address", "images", "id", "accommodation_type", "price",
                "created_at", "city", "district", "ward", "area"]

    def sort_condition(self, args):
        sortables = args.get('sort') or []
        sort_conditions = [self._get_sort_condition_from_param(sortable) for sortable in sortables]
        sort_conditions.append(self.sort_by_score())
        return sort_conditions

    def sort_by_score(self):
        return {
            '_score': {
                'order': 'desc'
            }
        }

    @staticmethod
    def build_extract_match_text_conditions(search_text: str, search_text_no_tone: str, args: dict) \
            -> List[query.Query]:
        fuzziness = "0"  # Don't use fuzzy in first query

        return [
            build_prefix_query(KEYWORD_NAME_FIELD, search_text, pow(10, 5) * 2),
            build_prefix_query(KEYWORD_NAME_NO_TONE_FIELD, search_text_no_tone, pow(10, 5)),

            build_phrase_prefix_query(SEARCH_TEXT_FIELD, search_text, pow(10, 3) * 2),
            build_phrase_prefix_query(
                SEARCH_TEXT_NO_TONE_FIELD, search_text_no_tone, pow(10, 3)),

            build_match_query(SEARCH_TEXT_FIELD, search_text, fuzziness, 2),
            build_match_query(SEARCH_TEXT_NO_TONE_FIELD, search_text, fuzziness)
        ]

    def get_filter_conditions(self, args):
        conditions = [query.MatchAll()]

        display_time_range = {
            'gte': 'now'
        }
        conditions.append(query.Nested(
            path='display_time_ranges',
            query=query.Range(display_time_ranges__end=display_time_range)
        ))

        owner_id = args.get('owner_id')
        if owner_id:
            conditions.append(query.Nested(
                path='owner',
                query=query.Term(owner__id=owner_id)
            ))

        city_code = args.get('city_code')
        if city_code:
            conditions.append(query.Nested(
                path='city',
                query=query.Term(city__code=city_code)
            ))

        district_code = args.get('district_code')
        if district_code:
            conditions.append(query.Nested(
                path='district',
                query=query.Term(district__code=district_code)
            ))

        ward_code = args.get('ward_code')
        if ward_code:
            conditions.append(query.Nested(
                path='ward',
                query=query.Term(ward__code=ward_code)
            ))

        # filter by price:
        price_range = {}
        price_max = args.get('price_max')
        if price_max:
            price_range['lte'] = price_max
        price_min = args.get('price_min')
        if price_min:
            price_range['gte'] = price_min
        if price_min or price_max:
            conditions.append(query.Range(price=price_range))

        electricity_price_range = {}
        electricity_price_max = args.get('electricity_price_max')
        if electricity_price_max:
            electricity_price_range['lte'] = electricity_price_max
        electricity_price_min = args.get('electricity_price_min')
        if electricity_price_min:
            electricity_price_range['gte'] = electricity_price_min
        if electricity_price_min or electricity_price_max:
            conditions.append(query.Range(electricity_price=electricity_price_range))

        water_price_range = {}
        water_price_max = args.get('water_price_max')
        if water_price_max:
            water_price_range['lte'] = water_price_max
        water_price_min = args.get('water_price_min')
        if water_price_min:
            water_price_range['gte'] = water_price_min
        if water_price_min or water_price_max:
            conditions.append(query.Range(water_price=water_price_range))

        is_stay_with_the_owner = args.get('is_stay_with_the_owner')
        if is_stay_with_the_owner:
            conditions.append(query.Term(is_stay_with_the_owner=is_stay_with_the_owner))

        has_electric_water_heater = args.get('has_electric_water_heater')
        if has_electric_water_heater:
            conditions.append(query.Term(has_electric_water_heater=has_electric_water_heater))

        has_balcony = args.get('has_balcony')
        if has_balcony:
            conditions.append(query.Term(has_balcony=has_balcony))

        has_air_conditioning = args.get('has_air_conditioning')
        if has_air_conditioning:
            conditions.append(query.Term(has_air_conditioning=has_air_conditioning))

        bathroom_type = args.get('bathroom_type')
        if bathroom_type:
            conditions.append(query.Term(bathroom_type=bathroom_type))

        kitchen_type = args.get('kitchen_type')
        if kitchen_type:
            conditions.append(query.Term(kitchen_type=kitchen_type))
        return conditions

    def _get_sort_condition_from_param(self, param):
        if param == 'last_updated':
            return {
                "updated_at": {
                    "order": "desc",
                    "missing": 0
                }
            }

        if param == 'price_asc':
            return {
                "price.price_sortable": {
                    "order": "asc",
                    "missing": 0
                }
            }

        if param == 'price_desc':
            return {
                "price.price_sortable": {
                    "order": "desc",
                    "missing": 0
                }
            }
