# coding=utf-8
import logging
import json

__author__ = 'nhatns'

from app.repositories.redis import RedisCacheBase
from app.repositories.mysql import ward as ward_repo, city as city_repo, district as district_repo

_logger = logging.getLogger(__name__)


def get_full_address_by_ward_code(ward_code: str):
    address_cache = AddressCache()
    return address_cache.get(ward_code)


def get_full_city():
    city_cache = CityCache()
    return city_cache.get('full')


def get_full_district_by_city_code(city_code: str):
    district_cache = DistrictCache()
    return district_cache.get(city_code)


def get_full_ward_by_district_code(district_code: str):
    ward_cache = WardCache()
    return ward_cache.get(district_code)


class AddressCache(RedisCacheBase):
    def __init__(self):
        super().__init__("address::name")

    def fetch_data(self, key: str):
        address = ward_repo.get_full_address_by_ward_code(key)
        return address


class CityCache(RedisCacheBase):
    def __init__(self):
        super().__init__("address::city")

    def fetch_data(self, key: str):
        cities = city_repo.get_all()
        return json.dumps([city.to_dict() for city in cities])


class DistrictCache(RedisCacheBase):
    def __init__(self):
        super().__init__("address::district")

    def fetch_data(self, key: str):
        districts = district_repo.get_all_by_city_code(key)
        return json.dumps([district.to_dict() for district in districts])


class WardCache(RedisCacheBase):
    def __init__(self):
        super().__init__("address::ward")

    def fetch_data(self, key: str):
        wards = ward_repo.get_all_by_district_code(key)
        return json.dumps([ward.to_dict() for ward in wards])
