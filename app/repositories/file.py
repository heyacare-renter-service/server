# coding=utf-8
import logging

__author__ = 'nhatns'

from flask import send_file, Response
from werkzeug.datastructures import MultiDict
from werkzeug.utils import secure_filename
import uuid

from app.models.mysql.image import Image
from app import models as m
from config import SEVER_BASE_URL

from app.extensions.custom_exception import CannotGetPictureException

_logger = logging.getLogger(__name__)


def generator_filename(filename):
    return filename + uuid.uuid4()


def upload_image(images: MultiDict) -> list:
    images = images.getlist('file')
    image_databases = []
    images_res = []
    for image in images:
        filename = secure_filename(str(uuid.uuid4()) + '-' + image.filename)
        path = SEVER_BASE_URL + 'file/images/' + filename
        images_res.append(path)
        image_databases.append(
            Image(content=image.read(), name=filename, content_type=image.content_type))

    m.db.session.add_all(image_databases)

    return images_res


def get_image(filename: str):
    image = Image.query.filter(
        Image.name == filename
    ).first()
    if not image:
        raise CannotGetPictureException()
    return Response(image.content, mimetype=image.content_type)

