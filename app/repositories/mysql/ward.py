import logging
from typing import List

from app.models.mysql.ward import Ward

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def get_full_address_by_ward_code(ward_code: str) -> str:
    ward = Ward.query.filter(Ward.code == ward_code).first()
    address = ward.name + ', ' + ward.district.name + ', ' + ward.district.city.name
    return address


def get_all_by_district_code(district_code: str) -> List[Ward]:
    return Ward.query.filter(Ward.district_code == district_code).all()


def get_by_code(code: str) -> Ward:
    return Ward.query.filter(Ward.code == code).first()
