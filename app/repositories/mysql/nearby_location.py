from app.models import NearByLocation


def delete_all_nearby_location_by_accommodation_id(accommodation_id: str):
    return NearByLocation.query.filter(NearByLocation.accommodation_id == accommodation_id).delete()
