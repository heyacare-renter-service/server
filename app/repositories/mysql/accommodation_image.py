from app.models import AccommodationImage


def delete_all_image_by_accommodation_id(accommodation_id: str):
    return AccommodationImage.query.filter(AccommodationImage.accommodation_id == accommodation_id).delete()
