import logging
from typing import List

from app import models as m
from app.models.mysql.owner import Owner, OwnerStatus
from sqlalchemy import func

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def save(register: Owner) -> Owner:
    m.db.session.add(register)
    m.db.session.commit()
    return register


def create_new_owner(user_id: int, status: int, identity_card_number) -> Owner:
    register = Owner(user_id=user_id, status=status, identity_card_number=identity_card_number)
    return save(register)


def find_pending_owner(page: int, limit: int) -> List[Owner]:
    owners = Owner.query.filter(OwnerStatus.Pending == Owner.status). \
        order_by(Owner.created_at.asc()). \
        limit(limit). \
        offset((page - 1) * limit)
    return owners


def get_number_pending_owner() -> int:
    return Owner.query.filter(OwnerStatus.Pending == Owner.status).count()


def find_pending_owner_by_ids(ids: List[int]) -> List[Owner]:
    owners = Owner.query.filter(Owner.status == OwnerStatus.Pending, Owner.user_id.in_(ids)).all()
    return owners
