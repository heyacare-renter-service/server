import logging
from typing import List

from app import models as m
from app.models.mysql.message import Message
from sqlalchemy import or_, and_


def save(message: Message):
    m.db.session.add(message)
    m.db.session.commit()
    return message


def save_message_to_database(**kwargs):
    message = Message(**kwargs)
    return save(message)


def get_messages_for_user(sender_id, receiver_id, page, limit):

    return Message.query.filter(or_(and_(Message.sender_id == sender_id, Message.receiver_id == receiver_id),
                                    and_(Message.sender_id == receiver_id, Message.receiver_id == sender_id))) \
        .order_by(Message.created_at.desc()) \
        .limit(limit) \
        .offset((page - 1) * limit)
