import logging
from typing import List
from datetime import datetime

from app import models as m
from sqlalchemy.sql import func
from sqlalchemy import desc

from app.models.mysql.accommodation import Accommodation, AccommodationStatus
from app.models.mysql.accommodation_type import AccommodationType
from app.models.mysql.accommodation_view import AccommodationView

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def get_accommodation_by_id(accommodation_id: str) -> Accommodation:
    return Accommodation.query.filter(Accommodation.id == accommodation_id,
                                      Accommodation.display_time >= datetime.now()).first()


def get_accommodation_by_owner_id(owner_id: int, page: int, limit: int) -> List[Accommodation]:
    return Accommodation.query.filter(Accommodation.owner_id == owner_id).limit(limit).offset((page - 1) * limit)


def count_accommodation_by_owner_id(owner_id: int):
    return Accommodation.query.filter(Accommodation.owner_id == owner_id).count()


def save(accommodation: Accommodation):
    m.db.session.add(accommodation)
    m.db.session.commit()
    return accommodation


def save_accommodation_to_database(**kwargs) -> Accommodation:
    accommodation = Accommodation(**kwargs)
    return save(accommodation)


def increase_accommodation_view(accommodation: Accommodation):
    accommodation.number_of_views += 1
    return save(accommodation)


def update_is_rented(accommodation_id: str, is_rented: bool):
    accommodation = get_accommodation_by_id(accommodation_id)
    accommodation.is_rented = is_rented
    save(accommodation)


def get_types() -> List[AccommodationType]:
    return AccommodationType.query.all()


def get_accommodation_for_owner_by_id(accommodation_id: str) -> Accommodation:
    return Accommodation.query.filter(Accommodation.id == accommodation_id).first()


def get_view_analysis(accommodation_id: str):
    res = m.db.session.query(func.count(AccommodationView.id).label('value'),
                             AccommodationView.date_of_access.label('time')) \
        .filter(AccommodationView.accommodation_id == accommodation_id).group_by(AccommodationView.date_of_access) \
        .order_by(AccommodationView.date_of_access) \
        .all()
    return [result._asdict() for result in res]


def get_all_view_analysis():
    res = m.db.session.query(func.count(AccommodationView.id).label('value'),
                             AccommodationView.date_of_access.label('time')) \
        .filter(True).group_by(AccommodationView.date_of_access) \
        .order_by(AccommodationView.date_of_access) \
        .all()
    return [result._asdict() for result in res]


def get_total_pending_accommodation():
    return Accommodation.query.filter(Accommodation.status == AccommodationStatus.Pending).count()


def get_pending_accommodation(page: int, limit: int) -> List[Accommodation]:
    return Accommodation.query.filter(Accommodation.status == AccommodationStatus.Pending) \
        .order_by(Accommodation.created_at) \
        .limit(limit) \
        .offset((page - 1) * limit)


def find_pending_accommodation_by_ids(ids: List[str]):
    return Accommodation.query.filter(Accommodation.status == AccommodationStatus.Pending,
                                      Accommodation.id.in_(ids)).all()


def find_accommodation_by_ids(ids: List[str]):
    return Accommodation.query.filter(Accommodation.id.in_(ids)).all()


def get_top10_view():
    top_views = m.db.session.query(func.count(AccommodationView.id).label('value'), AccommodationView.accommodation_id) \
        .filter(AccommodationView.accommodation.display_time >= func.now()) \
        .group_by(AccommodationView.accommodation_id) \
        .order_by(desc('value')) \
        .limit(10)
    ids = [view.accommodation_id for view in top_views]
    return find_accommodation_by_ids(ids)


def get_room_preview_by_id(accommodation_id: str) -> Accommodation:
    return Accommodation.query.filter(Accommodation.id == accommodation_id).first()
