import logging
from typing import List

from app.models.mysql.district import District

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def get_all_by_city_code(city_code: str) -> List[District]:
    return District.query.filter(District.city_code == city_code).all()


def get_by_code(code: str) -> District:
    return District.query.filter(District.code == code).first()
