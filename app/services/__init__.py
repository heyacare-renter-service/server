import logging
import threading

from flask_mail import Mail

from . import user
from . import accommodation
from . import admin
from . import favorite
from . import file
from . import mail_service
from . import owner
from . import register
from . import notification

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)

accommodation_lock = threading.Lock()
my_mail = Mail()


def init_app(app):
    my_mail.init_app(app)
