# coding=utf-8
import logging
from typing import List

from app import BadRequestException
from app.commons.decorators import user_required
from app.helpers.ultils import Utilities
from app.models import Rating
from app.repositories.mysql import rating as repo

__author__ = 'nhatns'

from app.repositories.redis.accommodation import RatingCache

_logger = logging.getLogger(__name__)


@user_required
def create_rating(star: int, accommodation_id: str, **kwargs):
    _rating_check(star)
    user = kwargs.get('user')
    comment = kwargs.get('comment', '')
    return repo.create_new_rating(user_id=user.id, comment=comment, accommodation_id=accommodation_id, star=star)


def find_rating(accommodation_id, **kwargs):
    args = Utilities.reformat_search_text_search_params(kwargs)
    star = args.get('star')
    if star:
        _rating_check(star)
        ratings = repo.find_all_rating_by_star(accommodation_id, star, args.get('_page'), args.get('_limit'))
    else:
        ratings = repo.find_all_rating(accommodation_id, args.get('_page'), args.get('_limit'))
    ratings = [rating.to_dict() for rating in ratings]
    return {'ratings': ratings, 'average_rating': get_average_rating(accommodation_id)}


def get_average_rating(accommodation_id: str):
    rating_cache = RatingCache()
    return rating_cache.get(accommodation_id)


def _rating_check(star: int):
    if star < 1 or star > 5:
        raise BadRequestException("Star must be between 1 and 5")


def get_pending_rating(page: int, limit: int) -> List[Rating]:
    return repo.find_pending_rating(page, limit)
