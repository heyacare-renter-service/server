import logging

from datetime import datetime, timedelta
from typing import List

from app import ForbiddenException
from app.commons.decorators import admin_or_owner_required
from app.helpers.ultils import Converter
from app.models.mysql.owner import Owner, OwnerStatus
from app.models.mysql.accommodation import Accommodation, KitchenType, BathroomType, AccommodationStatus
from app.models.mysql.accommodation_image import AccommodationImage
from app.models.mysql.nearby_location import NearByLocation
from app.models.mysql.user import User, UserRole
from app.repositories.mysql.accommodation_image import delete_all_image_by_accommodation_id
from app.repositories.mysql.nearby_location import delete_all_nearby_location_by_accommodation_id

from app.services import user as user_service
from app.services import ingestion as ingestion_service
from app.services import register as register_svc

from app.repositories.mysql import owner as repo
from app.repositories.mysql import accommodation as accommodation_repo

from app.repositories.redis import address as address_redis_repo

from app.extensions.custom_exception import AccommodationIsExisted, PermissionException, InvalidFieldException, \
    CannotCreateAccommodationException, AccommodationNotFound, CannotUpdateAccommodationException

from config import GOOGLE_API_KEY

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def register_to_be_owner(email: str, fullname: str, phone_number: str,
                         address: str, password: str, identity_card_number: str, **kwargs) -> Owner:
    register = register_svc.create_new_register(email=email, fullname=fullname, phone_number=phone_number,
                                                address=address,
                                                password=password, identity_card_number=identity_card_number)
    return register


def create_owner_to_database(user_id: int, identity_card_number: str):
    return repo.create_new_owner(user_id=user_id, identity_card_number=identity_card_number, status=OwnerStatus.Pending)


@admin_or_owner_required
def get_accommodations(_page, _limit, **kwargs) -> dict:
    owner = kwargs.get('user')
    print(owner.id)
    accommodations = accommodation_repo.get_accommodation_by_owner_id(owner.id, _page, _limit)
    accommodations = [accommodation.to_accommodation_owner_dict() for accommodation in accommodations]
    return {
        'total': accommodation_repo.count_accommodation_by_owner_id(owner_id=owner.id),
        'accommodations': accommodations
    }


@admin_or_owner_required
def create_accommodation(
        id: str, owner_id: int, title: str, description: str,
        street_address: str, ward_code: str, accommodation_type_id: int,
        number_of_room: int, price: int, area: int, is_stay_with_the_owner: bool,
        bathroom_type: int, kitchen_type: int, has_electric_water_heater: bool,
        has_air_conditioning: bool,
        has_balcony: bool, electricity_price: int, water_price: int,
        number_of_days_display: int, images: List[str], nearby_locations: List[str], **kwargs
) -> Accommodation:
    if check_accommodation_is_exist(id):
        raise AccommodationIsExisted()

    owner = kwargs.get('user')
    # validate data request
    validate_permission_of_owner(owner_id, owner)
    validate_kitchen_type(kitchen_type)
    validate_bathroom_type(bathroom_type)
    validate_images(images)

    # get status and time for accommodation
    status = AccommodationStatus.Pending
    approval_time = None
    display_time = None
    if owner.role == UserRole.Admin:
        status = AccommodationStatus.Approved
        approval_time = datetime.now()
        display_time = datetime.now() + timedelta(days=number_of_days_display)

    # try:
    #     location = get_geometry_location(
    #         street_address + ', ' + address_redis_repo.get_full_address_by_ward_code(ward_code=ward_code))
    # except:
    #     raise CannotCreateAccommodationException("Can get location of this accommodation")

    accommodation = Accommodation(
        id=id, owner_id=owner_id, title=title, description=description, street_address=street_address,
        ward_code=ward_code,
        accommodation_type_id=accommodation_type_id, number_of_room=number_of_room, price=price,
        area=area, is_stay_with_the_owner=is_stay_with_the_owner, bathroom_type=bathroom_type,
        kitchen_type=kitchen_type,
        has_electric_water_heater=has_electric_water_heater, has_air_conditioning=has_air_conditioning,
        has_balcony=has_balcony, electricity_price=electricity_price, water_price=water_price,
        number_of_days_display=number_of_days_display,
        images=[AccommodationImage(image_url=url, accommodation_id=id) for url in images],
        nearby_locations=[NearByLocation(name=name, accommodation_id=id) for name in nearby_locations],
        status=status,
        approval_time=approval_time,
        display_time=display_time,
        location_lat=0,
        location_lon=0
    )

    try:
        accommodation = accommodation_repo.save(accommodation)
    except:
        raise CannotCreateAccommodationException()

    accommodation_es = accommodation.to_elastic_dict()

    accommodation_es['images'] = _parse_images(images)
    accommodation_es['nearby_locations'] = _parse_nearby_locations(nearby_locations)

    Converter.reformat_accommodation(accommodation_es)
    ingestion_service.upsert_accommodation(accommodation_es)
    return accommodation


@admin_or_owner_required
def update_accommodation(
        id: str, owner_id: int, title: str, description: str,
        street_address: str, ward_code: str, accommodation_type_id: int,
        number_of_room: int, price: int, area: int, is_stay_with_the_owner: bool,
        bathroom_type: int, kitchen_type: int, has_electric_water_heater: bool,
        has_air_conditioning: bool,
        has_balcony: bool, electricity_price: int, water_price: int,
        number_of_days_display: int, images: List[str], nearby_locations: List[str], **kwargs
) -> Accommodation:
    accommodation = accommodation_repo.get_accommodation_for_owner_by_id(id)
    if not accommodation:
        raise AccommodationNotFound()

    owner = kwargs.get('user')
    # validate data request
    validate_permission_of_owner(owner_id, owner)
    if owner.role == UserRole.Owner and accommodation.status == AccommodationStatus.Approved:
        raise CannotUpdateAccommodationException()

    validate_kitchen_type(kitchen_type)
    validate_bathroom_type(bathroom_type)
    validate_images(images)

    status = AccommodationStatus.Pending
    approval_time = accommodation.approval_time
    display_time = accommodation.display_time
    if owner.role == UserRole.Admin:
        status = AccommodationStatus.Approved
        display_time = approval_time + timedelta(days=number_of_days_display)

    accommodation.title = title
    accommodation.description = description
    accommodation.accommodation_type_id = accommodation_type_id
    accommodation.kitchen_type = kitchen_type
    accommodation.bathroom_type = bathroom_type
    accommodation.has_balcony = has_balcony
    accommodation.price = price
    accommodation.area = area
    accommodation.is_stay_with_the_owner = is_stay_with_the_owner
    accommodation.electricity_price = electricity_price
    accommodation.water_price = water_price
    accommodation.has_electric_water_heater = has_electric_water_heater
    accommodation.has_air_conditioning = has_air_conditioning
    accommodation.ward_code = ward_code
    accommodation.street_address = street_address
    accommodation.number_of_room = number_of_room
    accommodation.number_of_days_display = number_of_days_display
    accommodation.status = status
    accommodation.approval_time = approval_time
    accommodation.display_time = display_time
    # delete all image and nearby location of accommodation
    delete_all_image_by_accommodation_id(accommodation_id=id)
    delete_all_nearby_location_by_accommodation_id(accommodation_id=id)
    # update new image, nearby location ...
    accommodation.images = [AccommodationImage(image_url=url, accommodation_id=id) for url in images]
    accommodation.nearby_locations = [NearByLocation(name=name, accommodation_id=id) for name in nearby_locations]

    try:
        accommodation = accommodation_repo.save(accommodation)
    except:
        raise CannotUpdateAccommodationException()

    accommodation_es = accommodation.to_elastic_dict()

    accommodation_es['images'] = _parse_images(images)
    accommodation_es['nearby_locations'] = _parse_nearby_locations(nearby_locations)

    Converter.reformat_accommodation(accommodation_es)
    ingestion_service.upsert_accommodation(accommodation_es)
    return accommodation


def check_accommodation_is_exist(accommodation_id: str) -> bool:
    accommodation = accommodation_repo.get_accommodation_for_owner_by_id(accommodation_id)
    return True if accommodation else False


def validate_permission_of_owner(owner_id, owner: User):
    if owner.id != owner_id:
        raise PermissionException("You can't create accommodation info of other owner")


def validate_kitchen_type(kitchen_type: int):
    if kitchen_type != KitchenType.Nothing and kitchen_type != KitchenType.Public and kitchen_type != KitchenType.Private:
        raise InvalidFieldException("Invalid field kitchen type")


def validate_bathroom_type(bathroom_type: int):
    if bathroom_type != BathroomType.NotClosed and bathroom_type != BathroomType.Closed:
        raise InvalidFieldException("Invalid field bathroom type")


def validate_images(images: List[str]):
    if len(images) < 3:
        raise InvalidFieldException("Must contain at least 3 photos")


def _parse_images(images_url: []):
    return [{'url': url} for url in images_url]


def _parse_nearby_locations(nearby_location: []):
    return [{'name': name} for name in nearby_location]


def get_geometry_location(address: str):
    geocode_result = gmaps.geocode(address)
    return geocode_result[0]['geometry']['location']


@admin_or_owner_required
def change_rental_status(is_rented: bool, accommodation_id: str, **kwargs):
    accommodation = accommodation_repo.get_accommodation_for_owner_by_id(accommodation_id)
    owner = kwargs.get('user')
    if owner.id is not accommodation.owner_id:
        raise PermissionException("You can't not change accommodation of other owner")
    accommodation.is_rented = is_rented
    return accommodation_repo.save(accommodation)


@admin_or_owner_required
def get_view_analysis(accommodation_id: str, **kwargs):
    accommodation = accommodation_repo.get_accommodation_for_owner_by_id(accommodation_id)
    owner = kwargs.get('user')
    if owner.id is not accommodation.owner_id:
        raise PermissionException("You can't not view analytics of other owner")
    return accommodation_repo.get_view_analysis(accommodation_id)


@admin_or_owner_required
def extend_display_time(accommodation_id: str, number_of_days: int, **kwargs):
    accommodation = accommodation_repo.get_accommodation_for_owner_by_id(accommodation_id=accommodation_id)
    if not accommodation:
        raise CannotUpdateAccommodationException()

    owner = kwargs.get('user')
    if owner.id is not accommodation.owner_id:
        raise PermissionException("You can't not view analytics of other owner")

    # get status and time for accommodation
    status = AccommodationStatus.Pending
    approval_time = None
    display_time = None
    if owner.role == UserRole.Admin:
        status = AccommodationStatus.Approved
        approval_time = datetime.now()
        display_time = datetime.now() + timedelta(days=number_of_days)

    accommodation.number_of_days_display = number_of_days
    accommodation.status = status
    accommodation.approval_time = approval_time
    accommodation.display_time = display_time

    try:
        accommodation = accommodation_repo.save(accommodation)
    except:
        raise CannotUpdateAccommodationException()

    accommodation_es = accommodation.to_elastic_dict()

    Converter.reformat_accommodation(accommodation_es)
    ingestion_service.upsert_accommodation(accommodation_es)
    return accommodation.to_elastic_dict()
