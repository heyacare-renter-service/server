import logging
from typing import List

from app.extensions.custom_exception import AccommodationNotFound
from app.helpers.ultils import Utilities
from app.models import Accommodation
from app.repositories.es.accommodation import AccommodationElasticRepo
from app.repositories.mysql import accommodation as repo, acommodation_view as view_repo

__author__ = 'nhatns'
_logger = logging.getLogger(__name__)


def get_accommodation_by_id(accommodation_id: str) -> dict:
    accommodation = repo.get_accommodation_by_id(accommodation_id)
    if accommodation:
        view_repo.save_view_to_database(accommodation_id)
        accommodation = repo.increase_accommodation_view(accommodation)
        return accommodation.to_dict()

    raise AccommodationNotFound()


def get_result_search(args):
    args = Utilities.reformat_accommodation_search_params(args)
    accommodation_es = AccommodationElasticRepo()
    response = accommodation_es.search(args)
    return extract_product_data_from_response(args, response)


def extract_product_data_from_response(args, responses):
    result = {
        'data': {
            'total': get_total_products(responses),
            'accommodations': extract_only_accommodations_from_response(responses)
        }
    }
    aggregations = args.get('aggregations') or []
    aggregations_result = {}
    if aggregations_result:
        result['data']['aggregations'] = aggregations_result

    return result


def extract_only_accommodations_from_response(responses):
    if not responses:
        return []
    hits = responses['hits']['hits']
    products = [item['_source'] for item in hits]
    return products


def get_total_products(response):
    total = response.get('hits') or {}
    total = total.get('total') or {}
    total = total.get('value') or 0
    return total


def get_type():
    return repo.get_types()


def get_pending_accommodation(page: int, limit: int) -> List[Accommodation]:
    return repo.get_pending_accommodation(page, limit)


def get_top10_view() -> List[dict]:
    accommodations = repo.get_top10_view()
    return [accommodation.to_top10_dict() for accommodation in accommodations]


def get_room_preview_by_id(accommodation_id: str) -> dict:
    accommodation = repo.get_room_preview_by_id(accommodation_id)
    if accommodation:
        return accommodation.to_dict()

    raise AccommodationNotFound()


def get_room_edit_by_id(accommodation_id: str) -> dict:
    accommodation = repo.get_room_preview_by_id(accommodation_id)
    if accommodation:
        return accommodation.to_edit_dict()

    raise AccommodationNotFound()