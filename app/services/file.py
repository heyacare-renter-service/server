# coding=utf-8
import logging

__author__ = 'nhatns'

from werkzeug.datastructures import MultiDict

from app.repositories import file as repo

_logger = logging.getLogger(__name__)


def upload_image(images: MultiDict):
    return {
        'images-url': repo.upload_image(images)
    }


def get_image(filename: str):
    return repo.get_image(filename)
