from app.models import Notification
from app.repositories.mysql import notification as repo


def create_notification(owner_id: int, accommodation_id: str, message: str):
    notification = Notification(owner_id=owner_id, accommodation_id=accommodation_id, message=message)
    return repo.save(notification)


def get_notifications_by_user_id(owner_id: int, page: int, limit: int):
    notifications = repo.get_notifications_for_user(owner_id, page, limit)
    return [notification.to_dict() for notification in notifications]


def get_number_unread(owner_id: int) -> int:
    return repo.get_number_notification_unread(owner_id)
