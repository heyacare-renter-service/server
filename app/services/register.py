import logging

from config import TOKEN_UPTIME, CLIENT_ADMIN_BASE_URL, CLIENT_BASE_URL
from app.extensions.custom_exception import RegisterBeforeException, UserExistsException
from app.helpers import encode_token, string_utils
from app.helpers import validate_register, hash_password
from app.models import Register
from app.repositories.mysql import register as repo
from app.services import mail_service, user as user_service
from app.templates import email_template

__author__ = 'nhat.ns'
_logger = logging.getLogger(__name__)


def create_new_register(email, fullname, password, **kwargs) -> Register:
    phone_number = string_utils.normalize_phone_number(kwargs.get('phone_number'))
    validate_register(email, phone_number, fullname, password)

    existed_user = user_service.find_one_by_email_or_phone_number(email, phone_number)
    if existed_user: raise UserExistsException()

    existed_pending_register = find_one_by_email_or_phone_number(email, password)
    if existed_pending_register:
        raise RegisterBeforeException()

    return repo.create_new_register(
        email=email,
        fullname=fullname,
        phone_number=phone_number,
        password=hash_password(password),
        gender=kwargs.get('gender') or 1,
        address=kwargs.get('address') or '',
        identity_card_number=kwargs.get('identity_card_number') or ''
    )


def send_confirm_email(email, fullname, **kwargs):
    confirm_token = encode_token(email, int(TOKEN_UPTIME))
    active_link = CLIENT_BASE_URL + 'register/confirm_email/' + confirm_token
    message_html = email_template.gen_confirm_email_body_template(fullname, email, active_link)
    mail_service.send_email("HeyaCare's - Thư xác nhận đăng ký thành viên", email, message_html)


def send_confirm_owner_email(email, fullname, **kwargs):
    confirm_token = encode_token(email, int(TOKEN_UPTIME))
    active_link = CLIENT_ADMIN_BASE_URL + 'register/confirm_email/' + confirm_token
    message_html = email_template.gen_confirm_email_body_template(fullname, email, active_link)
    mail_service.send_email("HeyaCare's - Thư xác nhận đăng ký tài khoản chủ trọ", email, message_html)


def delete_one_by_email(email: str):
    repo.delete_one_by_email(email)


def find_one_by_email(email: str) -> Register:
    return repo.find_one_by_email(email)


def find_one_by_phone_number(phone_number: str) -> Register:
    return repo.find_one_by_phone_number(phone_number)


def find_one_by_email_or_phone_number(email: str, phone_number: str):
    return repo.find_one_by_email_or_phone_number(email, phone_number) \
        if phone_number else repo.find_one_by_email(email)
