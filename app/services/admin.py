import logging
from datetime import datetime, timedelta
from typing import List

from app import services
from app.commons.decorators import admin_required
from app.helpers.ultils import Utilities, Converter
from app.models.mysql.accommodation import AccommodationStatus
from app.models.mysql.owner import OwnerStatus
from app.models.mysql.rating import RatingStatus
from app.models.mysql.user import UserRole
from app.repositories.mysql import accommodation as accommodation_repo
from app.repositories.mysql import owner as owner_repo
from app.repositories.mysql import rating as rating_repo
from app.services import ingestion as ingestion_service
from app.services import mail_service
from app.services import notification as notification_svc

__author__ = 'nhatns'

from app.services.owner import _parse_images, _parse_nearby_locations
from app.templates import email_template

_logger = logging.getLogger(__name__)


@admin_required
def get_pending_owner(args, **kwargs) -> List[dict]:
    args = Utilities.reformat_search_text_search_params(args)
    owners = owner_repo.find_pending_owner(args.get('_page'), args.get('_limit'))
    return [owner.to_dict() for owner in owners]


def get_number_pending_owner() -> int:
    return owner_repo.get_number_pending_owner()


@admin_required
def confirm_owner_req(ids: List[int], **kwargs):
    owners = owner_repo.find_pending_owner_by_ids(ids)
    if not owners:
        return "Can't confirm for any owners"
    message = "Confirm for owners with ids: "
    new_ids = []
    for owner in owners:
        owner.status = OwnerStatus.Approved
        owner.user.role = UserRole.Owner
        owner_repo.save(owner)
        # sent email to owner if confirm success.
        message = email_template.gen_notify_owner_has_approved(owner.user.fullname, owner.user.email)
        mail_service.send_email('Thư chúc mừng đăng ký thành công tài khoản chủ trọ - HeyaCare team', owner.user.email,
                                message)

        new_ids.append(str(owner.user_id))
    message += ' , '.join(new_ids)
    return message


@admin_required
def get_pending_rating(args, **kwargs) -> List[dict]:
    args = Utilities.reformat_search_text_search_params(args)
    ratings = services.rating.get_pending_rating(args.get('_page'), args.get('_limit'))
    ratings = [rating.to_dict() for rating in ratings]
    return ratings


def get_total_pending_rating() -> int:
    return rating_repo.get_total_pending_rating()


def confirm_rating_req(ids: List[int], **kwargs):
    ratings = rating_repo.find_pending_rating_by_ids(ids)
    if not ratings:
        return "Can't confirm for any ratings"
    message = "Confirm for ratings with ids: "
    new_ids = []
    for rating in ratings:
        rating.status = RatingStatus.Approved
        rating_repo.save(rating)
        # notify to owner rating has been accepted
        notification_svc.create_notification(accommodation_id=rating.accommodation_id,
                                             owner_id=rating.accommodation.owner_id,
                                             message=rating.user.fullname + " đã đánh giá bài viết của bạn."
                                             )
        new_ids.append(str(rating.id))
    message += ' , '.join(new_ids)
    return message


def remove_rating_req(ids: List[int]):
    ratings = rating_repo.find_pending_rating_by_ids(ids)
    if not ratings:
        return "Can't block for any ratings"
    message = "Block for ratings with ids: "
    new_ids = []
    for rating in ratings:
        rating.status = RatingStatus.Block
        rating_repo.save(rating)
        new_ids.append(str(rating.id))
    message += ' , '.join(new_ids)
    return message


@admin_required
def get_pending_accommodation(args, **kwargs) -> List[dict]:
    args = Utilities.reformat_search_text_search_params(args)
    accommodations = services.accommodation.get_pending_accommodation(args.get('_page'), args.get('_limit'))
    accommodations = [accommodation.to_accommodation_admin_dict() for accommodation in accommodations]
    return accommodations


def get_total_pending_accommodation():
    return accommodation_repo.get_total_pending_accommodation()


def confirm_accommodation_req(ids: List[str]):
    accommodations = accommodation_repo.find_pending_accommodation_by_ids(ids)
    if not accommodations:
        return "Can't confirm for any accommodations"
    message = "Confirm for accommodations with ids: "
    new_ids = []
    for accommodation in accommodations:
        accommodation.status = AccommodationStatus.Approved
        accommodation.approval_time = datetime.now()
        accommodation.display_time = datetime.now() + timedelta(days=accommodation.number_of_days_display)
        accommodation = accommodation_repo.save(accommodation)

        # notify to owner post has been accepted
        notification_svc.create_notification(accommodation_id=accommodation.id,
                                             owner_id=accommodation.owner_id,
                                             message="Bài đăng " + accommodation.title + " đã được quản trị viên phê "
                                                                                         "duyệt. Sẵn sàng hiển thị "
                                                                                         "trên trang chủ trong thời "
                                                                                         "gian từ " +
                                                     accommodation.approval_time.strftime(
                                                         '%d/%m/%Y') + " đến " + accommodation.display_time.strftime(
                                                 "%d/%m/%Y") + "."
                                             )

        accommodation_es = accommodation.to_elastic_dict()

        accommodation_es['images'] = _parse_images([image.get_url() for image in accommodation.images])
        accommodation_es['nearby_locations'] = _parse_nearby_locations(
            location.get_name() for location in accommodation.nearby_locations)

        Converter.reformat_accommodation(accommodation_es)
        ingestion_service.upsert_accommodation(accommodation_es)
        new_ids.append(str(accommodation.id))
    message += ' , '.join(new_ids)
    return message


def decline_accommodation_req(ids: List[str]):
    accommodations = accommodation_repo.find_pending_accommodation_by_ids(ids)
    if not accommodations:
        return "Can't decline for any accommodations"
    message = "Decline for accommodations with ids: "
    new_ids = []
    for accommodation in accommodations:
        accommodation.status = AccommodationStatus.Block
        accommodation_repo.save(accommodation)
        notification_svc.create_notification(accommodation_id=accommodation.id,
                                             owner_id=accommodation.owner_id,
                                             message="Bài đăng " + accommodation.title + " đã bị quản trị viên từ chối."
                                             )
        new_ids.append(str(accommodation.id))

    message += ' , '.join(new_ids)
    return message


def get_view_analysis():
    return accommodation_repo.get_all_view_analysis()
