import logging

__author__ = 'nhat.ns'
_logger = logging.getLogger(__name__)


def gen_confirm_email_body_template(fullname: str, username: str, active_link: str):
    fullname = "" if not fullname else (' ' + fullname)

    message_html = ('<p>Xin chào{}, </p>'
                    '<p>Bạn đã hoàn toàn sẵn sàng với HeyaCare qua email {}.</p>'
                    "<p>Nhấn vào <a href= '{}' >link</a> để hoàn tất đăng ký!</p>"
                    '<p>Thân ái,</p>'
                    "<p>HeyaCare team</p>"
                    )
    message_html = message_html.format(fullname, username, active_link)
    return message_html


def gen_notify_owner_has_approved(fullname: str, username: str):
    fullname = "" if not fullname else (' ' + fullname)
    message_html = ('<p>Xin chào{}, </p>'
                    '<p>Chúc mừng bạn đã trở thành một phần của HeyaCare với email {}.</p>'
                    "<p>Từ bây giờ bạn có thể đăng bài và quản lý phòng trọ của mình, đưa phòng trọ của mình tiếp cận "
                    "với hơn 10.000 người dùng trên trang. Hãy tận dụng cơ hội này và cùng HeyaCare tạo nên nhiều "
                    "điều khác biệt nhé <3!</p> "
                    '<p>Thân ái,</p>'
                    "<p>HeyaCare team</p>"
                    )
    message_html = message_html.format(fullname, username)
    return message_html
